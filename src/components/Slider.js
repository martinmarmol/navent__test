import React from 'react';
import PropTypes from 'prop-types';
import './Slider.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

/* PARA LABURAR EL SAVE:
	window.localStorage
	window.localStorage.setItem('user1:aviso1:favorito', true)
	window.localStorage.getItem('user1:aviso1:favorito')
*/

class Favorite extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selected: false
		};

		this.handleChange = this.handleChange.bind(this);
	}

	componentDidMount() {
		const selected = localStorage.getItem("selected") === 'true';
		this.setState({
			selected: selected
		});
	}
	
	handleChange() {
		this.setState({
			selected: !this.state.selected
		});
		localStorage.setItem("selected", !this.state.selected);
	}

	render() {
		const selected = this.state.selected;
		
		return (
			<FontAwesomeIcon onClick={this.handleChange} className={`slider__favorite ${selected ? 'slider__favorite_selected' : ''}`} icon="heart" />
		);
	}
}
	
function CarouselItems(props) {
	const images = props.images;

	return images.map((image, i) => {
		return (
			<div className={`carousel-item ${i === 0 ? 'active' : ''}`} key={i}>
				<img className="d-block w-100" src={image} alt={`Slide number ${i+1}`}/>
			</div>
		);
	});
}

CarouselItems.propTypes = {
  images: PropTypes.array.isRequired
}

function Slider(props) {
	const images = props.images;
	const tag = props.tag;
	
	let tagMessage;
	tag 
		? tagMessage = <span className='slider__tag'><strong>{tag}</strong></span>
		: tagMessage = null
	
	return (
		<div id="carouselExampleControls" className={`carousel slide ${props.className}`} data-ride="carousel">
			{tagMessage}
			<Favorite/>
			<div className="carousel-inner">
				<CarouselItems images={images}/>
			</div>
			<a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				<span className="carousel-control-prev-icon" aria-hidden="true"></span>
				<span className="sr-only">Anterior</span>
			</a>
			<a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				<span className="carousel-control-next-icon" aria-hidden="true"></span>
				<span className="sr-only">Siguiente</span>
			</a>
		</div>
	);
}

Slider.propTypes = {
	images: PropTypes.array.isRequired,
	tag: PropTypes.string,
	className: PropTypes.string
}

export default Slider;