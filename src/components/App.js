import React, { Component } from 'react';
import $ from "jquery";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Slider from './Slider';
import Price from './Price';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt, faHeart } from '@fortawesome/free-solid-svg-icons'
import aviso__img_1 from '../img/aviso__img_1.jpg';
import aviso__img_2 from '../img/aviso__img_2.jpg';
import aviso__img_3 from '../img/aviso__img_3.jpg';
library.add(faMapMarkerAlt, faHeart);


const images = [aviso__img_1, aviso__img_2, aviso__img_3];

class Modal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: false,
			emailAddress: ''
		};
		this.handleInput = this.handleInput.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInput(e) {
		console.log(e.target.value);
		
		this.setState({
			emailAddress: e.target.value.toLowerCase(),
		});
	}

	handleSubmit(e) {
		e.preventDefault();
		const emailAddress = this.state.emailAddress;
		
		function validateEmail(email) {
			const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}

		if (validateEmail(emailAddress)){
			$('#exampleModal').modal('hide');
			this.setState({
				emailAddress: '',
				error: false
			});
		} else {
			this.setState({
				error: true
			});
		}
		
	}

	render() {
		const emailAddress = this.state.emailAddress;
		const error = this.state.error;

		return (
			<div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div className="modal-dialog" role="document">
					<div className="modal-content">
						<form formNoValidate={true} id='contact-form' onSubmit={this.handleSubmit}>
							<div className="modal-header">
								<h1 className="modal-title">Para ser contactado por favor ingrese su direccion de correo electronico.</h1>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div className="modal-body">
							<label className="aviso__modal-label" form='contact-form' htmlFor='contact-form__input' id="exampleModalLabel">Email</label>
								<input formNoValidate={true} className='aviso__modal-input' value={emailAddress} onChange={this.handleInput} placeholder='martin@ejemplo.com' type='email' id='contact-form__input' name="email" required={true}/>
								<p className={`aviso__modal-input-error ${error ? 'aviso__modal-input-error_shown' : ''}`}>Por favor ingresar un email válido</p>
							</div>
							<div className="modal-footer">
								<button className="btn btn-primary" type="submit" value="Submit">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

class Anuncio extends Component {
	constructor(props) {
		super(props);
		this.state = {
			avisoContainerRes: null
		};
	}

	componentDidMount() {
		const avisoContainer = document.querySelector('.aviso__container');
		let avisoContainerWidth = $(avisoContainer).width();

		this.setState({
			avisoContainerRes: avisoContainerWidth
		});

		const updateSize = () => {
			avisoContainerWidth = $(avisoContainer).width()
			
			this.setState({
				avisoContainerRes: avisoContainerWidth
			});
		};

		window.addEventListener('resize', updateSize);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', () => { console.log('Removed resize event listener') });
	}

	render() {
		const avisoContainerRes = this.state.avisoContainerRes;
		const horizontalLayout = avisoContainerRes > 413;

		return (
			<div className='aviso__container'>
				<div className={ `aviso shadow ${horizontalLayout ? 'aviso_horizontal' : 'aviso_vertical'}` }>
					<Slider className='aviso__slider' images={images} tag='Súper Destacado'/>
					<div className='aviso__body'>
						<header className='aviso__header'>
							<h1 className='aviso__title h5 '><a href='http://coso.com'>Monoambiente Divisible Luminoso</a></h1>
							<small className="aviso__location text-muted"><FontAwesomeIcon icon="map-marker-alt" /><span style={{marginLeft: '5px'}}>Av. San Martin 1700 - Caballito, Capital Federal</span></small>
						</header>
						<main className='aviso__main'>
							<p className='aviso__description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<Price meters={86} price={1200478} className={horizontalLayout ? '' : 'price_vertical'}/>
						</main>
						<footer className={`aviso__footer row no-gutters justify-content-between ${horizontalLayout ? '' : 'aviso__footer_vertical'}`}>
							<ul className='aviso__features list-inline col-auto'>
								<li className='list-inline-item'>
									<strong>86 m²</strong>
								</li>
								<li className='list-inline-item'>
									<strong>2 Ambientes</strong>
								</li>
								<li className='list-inline-item'><strong>
									1 Baño
								</strong></li>
							</ul>
							<button type='text' className='aviso__contact col-auto' data-toggle="modal" data-target="#exampleModal"><strong>CONTACTAR</strong></button>
						</footer>
					</div>
				</div>
				<Modal />
			</div>
		);
	}
}


export default Anuncio;