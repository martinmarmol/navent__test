import React from 'react';
import PropTypes from 'prop-types';
import numeral from 'numeral';
import './Price.css';

/* PARA LABURAR EL SAVE:
	window.localStorage
	window.localStorage.setItem('user1:aviso1:favorito', true)
	window.localStorage.getItem('user1:aviso1:favorito')
*/

class Price extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			price: this.props.price,
			editable: false
		};

		this.handleEditable = this.handleEditable.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
	}

	componentDidMount() {
		const storedPrice = Number(localStorage.getItem("price"));
		storedPrice && this.setState({price: storedPrice});
	}

	handleChange(e) {
		const neWvalue = e.target.value < 1000000000 ? e.target.value : 999999999;
		
		this.setState({
			price: neWvalue,
		});
		localStorage.setItem("price", neWvalue);
	}

	handleKeyDown(e) {
		if (e.keyCode === 27 || e.keyCode === 13) {
			this.setState({
				editable: !this.state.editable
			});
		}
	}

	handleEditable() {
		this.setState({
			editable: !this.state.editable
		});
	}

	render() {
		const editable = this.state.editable;
		const meters = this.props.meters;
		const price = numeral(this.state.price).format('0,0');
		const squarePrice = numeral(this.state.price / this.props.meters).format('0,0');
		let priceField;

		editable 
			? priceField = <input value={this.state.price} onChange={this.handleChange} onKeyDown={this.handleKeyDown} onBlur={this.handleEditable} type='number' max='999999999' min={meters} maxLength="9" className='price__input h3 font-weight-bold' />
			: priceField = <button onClick={this.handleEditable} className='price__label h3 font-weight-bold'>U$S {price} </button>

		return (
			<section className={`price ${this.props.className}`}>
				{priceField}
				<span className='price__square-meters'><strong> $/m² {squarePrice}</strong></span>
			</section>
		)
	}
}

Price.propTypes = {
  price: PropTypes.number.isRequired,
  meters: PropTypes.number.isRequired,
  className: PropTypes.string
}

export default Price;